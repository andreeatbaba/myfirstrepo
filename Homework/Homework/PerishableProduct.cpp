#include "PerishableProduct.h"
PerishableProduct::PerishableProduct(int32_t id, std::string name, float rawPrice, std::string expirationDate) :
	Product::Product(id, name, rawPrice),
	m_expirationDate(expirationDate)
{
	// empty
}

int32_t PerishableProduct::GetVat() const
{
	return kVAT;
}

float PerishableProduct::GetPrice() const
{
	return m_rawPrice + m_rawPrice * GetVat() / 100.0f;
}

const std::string& PerishableProduct::GetExpirationDate() const
{
	return m_expirationDate;
}
