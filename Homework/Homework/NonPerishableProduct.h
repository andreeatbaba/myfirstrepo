#pragma once
#include "Product.h"
class NonperishableProduct : public Product
{
public:
	enum class Type
	{
		Clothing,
		SmallAppliances,
		PersonalHygiene
	};

public:
	NonperishableProduct(int32_t id, std::string name, float rawPrice, Type type);

	// Inherited via Product
	int32_t GetVat() const override;
	float GetPrice() const override;

	friend std::ostream& operator << (std::ostream& os, const NonperishableProduct& prod);

private:
	static const int32_t kVat = 19;

private:
	Type m_type;
}

