#pragma once
#include <cstdint> //The library for int16_t, int32_t, etc.

class IPriceable
{
	virtual ~IPriceable() = default;
	virtual int32_t GetVat() const = 0;
	virtual float GetPrice() const = 0;
};

