#pragma once
#include "IPriceable.h"
#include <string>

class Product : public IPriceable
{
public:
	Product(const int32_t& id, const std::string& name, const float& rawPrice) ;
	// This are the getters for the elements of the class
	int32_t GetID() const;
	const std::string& GetName() const;
	float GetRawPrice() const;

protected:

	//the elements
	int32_t m_id;
	std::string m_name;
	float m_rawPrice;
};


