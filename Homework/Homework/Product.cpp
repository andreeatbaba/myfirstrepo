#include "Product.h"

Product::Product(const int32_t& id, const std::string& name, const float& rawPrice) :
	m_id(id),
	m_name(name),
	m_rawPrice(rawPrice)
{

}

int32_t Product::GetID() const
{
	return m_id;
}

const std::string& Product::GetName() const
{
	return m_name;
}

float Product::GetRawPrice() const
{
	return m_rawPrice;
}
